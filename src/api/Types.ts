export interface Tag {
  name: string;
}

export interface RecipeListItem {
  id: string;
  photoAssetUrl: string;
  chefAssetName: string;
  tagsAsset: Tag[];
  title: string;
  description: string;
  calories: string;
}
