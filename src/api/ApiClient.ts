import { ContentfulClientApi, createClient } from "contentful";
import { RecipeListItem } from "./Types";

export class ApiClient {
  // undefined cuz of definition
  // export declare function createClient(params: CreateClientParams): ContentfulClientApi<undefined>;
  private client: ContentfulClientApi<undefined>;

  constructor() {
    this.client = createClient({
      // This is the space ID. A space is like a project folder in Contentful terms
      space: "kk2bw5ojx476",
      // This is the access token for this space. Normally you get both ID and the token in the Contentful web app
      accessToken:
        "7ac531648a1b5e1dab6c18b0979f822a5aad0fe5f1109829b8a197eb2be4b84c",
    });
  }

  protected async getRecipes(): Promise<RecipeListItem[]> {
    return this.client
      .getEntries({
        content_type: "recipe",
      })
      .then((data) => {
        return data.items.map((item) => ({
          id: item.sys.id,
          photoAssetUrl: this.findPhoto(
            this.findAsset(
              (item.fields["photo"] as any)?.sys.id,
              data.includes?.Asset
            )
          ) as string,
          chefAssetName: this.findChef(
            this.findEntry(
              (item.fields["chef"] as any)?.sys.id,
              data.includes?.Entry
            )
          ) as string,
          tagsAsset: this.findTags((item.fields["tags"] as any) ?? []),
          title: item.fields["title"] as string,
          description: item.fields["description"] as string,
          calories: item.fields["calories"] as string,
        }));
      });
  }

  private findPhoto(asset: any) {
    return `http:${asset.fields.file.url}`;
  }

  private findAsset(assetId?: string, assets?: any[]) {
    return assets?.find((asset) => asset.sys.id === assetId);
  }

  private findChef(entry: any) {
    return entry?.fields?.name;
  }

  private findEntry(entryId?: string, entries?: any[]) {
    return entries?.find((entry) => entry.sys.id === entryId);
  }

  private findTags(tags: any[]) {
    return tags.map((tag) => {
      return {
        name: tag.fields.name,
      };
    });
  }
}
