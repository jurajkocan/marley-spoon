import React from "react";
import ReactDOM from "react-dom/client";
import { RouterProvider } from "react-router-dom";
import router from "./Router";
import ThemeProvider from "./theme/ThemeProvider";
import "./index.css";
import { StoreContext, store } from "./store/_Store";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <StoreContext.Provider value={store}>
      <ThemeProvider>
        <RouterProvider router={router} />
      </ThemeProvider>
    </StoreContext.Provider>
  </React.StrictMode>
);
