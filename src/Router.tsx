import {
  createBrowserRouter,
  createRoutesFromElements,
  Navigate,
  Route,
} from "react-router-dom";

import RecipeList from "./pages/RecipeList";
import RecipeDetail from "./pages/RecipeDetail";
import MasterPage from "./pages/MasterPage";

export const Routes = {
  DEFAULT: "/",
  RECIPE_LIST: "/list",
  RECIPE_DETAIL: "/detail",
  RECIPE_DETAIL_WITH_PARAM: "/detail/:id",
};

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route element={<MasterPage />}>
      <Route
        path={Routes.DEFAULT}
        element={<Navigate to={Routes.RECIPE_LIST} replace={true} />}
      />
      <Route path={Routes.RECIPE_LIST} element={<RecipeList />} />
      <Route
        path={Routes.RECIPE_DETAIL_WITH_PARAM}
        element={<RecipeDetail />}
      />
    </Route>
  )
);

export default router;
