import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { useStore } from "../store/_Store";
import RecipeCard from "../components/RecipeCard";

const RecipeList = observer(() => {
  const { recipeStore } = useStore();

  useEffect(() => {
    recipeStore.getAllRecipes();
  }, [recipeStore]);

  if (recipeStore.recipesLoading) {
    return <>Loading...</>;
  }

  return (
    <div className="">
      {recipeStore.recipes.map((recipe, index) => {
        return (
          <div key={`recipe-list-item-${index}`} className="mb-5">
            <RecipeCard recipe={recipe} />
          </div>
        );
      })}
    </div>
  );
});

export default RecipeList;
