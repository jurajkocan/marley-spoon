import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import Tags from "../components/Tags";
import { useStore } from "../store/_Store";

const RecipeDetail = observer(() => {
  const { recipeStore } = useStore();
  const { id } = useParams<string>();

  useEffect(() => {
    recipeStore.getRecipeDetail(id ?? "");
  }, []);

  return (
    <div className="max-w-7xl flex flex-col md:flex-row gap-8">
      <div className="relative">
        <img
          className="object-contain w-150 max-h-[50rem] h-auto"
          src={`${recipeStore.recipeDetail?.photoAssetUrl}`}
          srcSet={`${recipeStore.recipeDetail?.photoAssetUrl} 1020w`}
        />
        <h1 className="max-w-xs absolute top-5 mx-3 text-white bg-[#000000ba] p-4 rounded-md">
          {recipeStore.recipeDetail?.title}
        </h1>
      </div>

      <div className="flex flex-col gap-2">
        <article className="prose lg:prose-xl">
          {recipeStore.recipeDetail?.description}
        </article>
        <div className="italic text-sm">
          {recipeStore.recipeDetail?.chefAssetName}
        </div>
        {recipeStore.recipeDetail?.tagsAsset && (
          <Tags tags={recipeStore.recipeDetail?.tagsAsset} />
        )}
      </div>
    </div>
  );
});

export default RecipeDetail;
