import { Outlet } from "react-router-dom";
import NavBar from "../components/Navbar";

const MasterPage = () => {
  return (
    <>
      <NavBar />
      <div className="p-4 pt-20 grid justify-center">
        <Outlet />
      </div>
    </>
  );
};

export default MasterPage;
