import type { ButtonStyleTypes } from "@material-tailwind/react";

interface Theme {
  button: ButtonStyleTypes;
}

export const theme: Theme = {
  button: {
    defaultProps: {
      color: "green",
    },
  },
};
