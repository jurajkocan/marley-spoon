import { ThemeProvider as MaterialThemeProvider } from "@material-tailwind/react";
import { theme } from "./Theme";

export default function ThemeProvider(props: React.PropsWithChildren) {
  return (
    <MaterialThemeProvider value={theme}>
      {props.children}
    </MaterialThemeProvider>
  );
}
