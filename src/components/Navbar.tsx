import { Navbar, Typography } from "@material-tailwind/react";
import { Link } from "react-router-dom";
import { Routes } from "../Router";

const NavBar = () => {
  return (
    <Navbar
      variant="filled"
      blurred={false}
      fullWidth={true}
      className="max-w-full w-full p-2 top-0 lg:pl-6 fixed z-10 bg-black"
    >
      <Typography className="mr-4 ml-2 cursor-pointer py-1.5 font-medium">
        <Link
          to={Routes.DEFAULT}
          className="flex gap-2 text-white items-center"
        >
          JK-FE
        </Link>
      </Typography>
    </Navbar>
  );
};

export default NavBar;
