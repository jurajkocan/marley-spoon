import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Typography,
} from "@material-tailwind/react";
import { Link } from "react-router-dom";
import { RecipeListItem } from "../api/Types";
import { Routes } from "../Router";

type Props = {
  recipe: RecipeListItem;
};

const RecipeCard = (props: Props) => {
  return (
    <Link to={`${Routes.RECIPE_DETAIL}/${props.recipe.id}`}>
      <Card className="w-150 max-w-4xl overflow-hidden">
        <CardHeader floated={false} className="m-0 rounded-none max-h-[50rem]">
          <img
            className="object-contain w-150 max-h-[50rem] h-auto"
            src={`${props.recipe.photoAssetUrl}`}
            srcSet={`${props.recipe.photoAssetUrl} 1020w`}
          />
        </CardHeader>

        <CardBody className="text-center">
          <Typography variant="h5" color="blue-gray" className="mb-2 text-left">
            {props.recipe.title}
          </Typography>
        </CardBody>
        <CardFooter className="flex justify-start gap-7 pt-2">
          <Button
            onClick={() => {}}
            className="pointer-events-auto w-full md:w-auto"
          >
            Read More
          </Button>
        </CardFooter>
      </Card>
    </Link>
  );
};

export default RecipeCard;
