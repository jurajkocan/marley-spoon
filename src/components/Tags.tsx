import { Chip } from "@material-tailwind/react";
import { Tag } from "../api/Types";

interface Props {
  tags: Tag[];
}

const Tags = (props: Props) => {
  return (
    <div className="flex gap-2">
      {props.tags.map((tag, index) => (
        <Chip key={`recipe-tag-${index}`} variant="filled" value={tag.name} />
      ))}
    </div>
  );
};

export default Tags;
