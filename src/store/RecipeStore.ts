import { action, makeObservable, observable, runInAction } from "mobx";
import { ApiClient } from "../api/ApiClient";
import { RecipeListItem } from "../api/Types";

class RecipeStore extends ApiClient {
  @observable
  recipes: RecipeListItem[] = [];

  @observable
  recipesLoading = false;

  @observable
  recipeDetail: RecipeListItem | null = null;

  constructor() {
    super();
    makeObservable(this);
  }

  @action
  async getAllRecipes() {
    this.recipesLoading = true;
    try {
      const recipes = await this.getRecipes();
      runInAction(() => {
        this.recipes = recipes;
      });
    } finally {
      runInAction(() => {
        this.recipesLoading = false;
      });
    }
  }

  @action
  getRecipeDetail(id: string) {
    this.recipeDetail = this.recipes.find((recipe) => recipe.id === id) ?? null;
  }
}

export default RecipeStore;
