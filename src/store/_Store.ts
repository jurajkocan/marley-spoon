import { createContext, useContext } from "react";
import RecipeStore from "./RecipeStore";

export interface IStore {
  recipeStore: RecipeStore;
}

export const store: IStore = {
  recipeStore: new RecipeStore(),
};

export const StoreContext = createContext(store);
export const useStore = () => {
  return useContext(StoreContext);
};
